var i18n_vi_data = {
   "domain": "messages",
   "locale_data": {
      "messages": {
         "": {
            "domain": "messages",
            "plural_forms": "nplurals=1; plural=0;",
            "lang": "vi"
         },
         "Page not found": [
            "Không tìm thấy trang yêu cầu"
         ],
         "The page you're looking for does not exists.": [
            "Chúng tôi không thể tìm thấy trang bạn yêu cầu."
         ],
         "Here's a cute Hatsune Miku because we like it.<br />Not really related, but whatever.": [
            "Đây là Hatsune Miku vì chúng tôi thích vậy. <br />Cũng chẳng liên quan tới cái báo lỗi này đâu, mà thôi kệ."
         ],
         "Bad request": [
            "Yêu cầu không đúng chuẩn"
         ],
         "Your request couldn't be understood by the server.": [
            "Máy chủ không thể hiểu yêu cầu của bạn."
         ],
         "Patchouli Knowledge is not happy that you might be doing nasty things to the server.": [
            "Patchouli Knowledge có vẻ không vui lắm vì bạn đang táy máy gì đó với máy chủ thì phải."
         ],
         "Forbidden": [
            "Tài nguyên cấm truy cập"
         ],
         "You just tried to access something which you don't have the required permission to do so.": [
            "Bạn không có quyền truy cập vào trang mà bạn đã yêu cầu."
         ],
         "Internal Server Error": [
            "Lỗi máy chủ"
         ],
         "The server encountered an internal error and was unable to complete your request.": [
            "Máy chủ đã gặp lỗi trong quá trình hoàn thành yêu cầu của bạn."
         ],
         "Flandre Scarlet is sad because she couldn't fulfill this request <br />(don't worry though, we have been notified of this issue and will try to fix this issue as soon as possible)": [
            "Flandre Scarlet đang rất buồn vì cô ấy không thể hoàn thành yêu cầu của bạn. <br />(Đừng lo, chúng tôi đã nhận được thông báo về việc này và sẽ khắc phục trong thời gian ngắn nhất)"
         ],
         "Announcements": [
            "Thông báo"
         ],
         "Contest": [
            "Cuộc thi"
         ],
         "Login as": [
            "Đăng nhập dưới danh nghĩa"
         ],
         "Username": [
            "Tên đăng nhập"
         ],
         "Questions in set %(qset_name)s": [
            "Câu hỏi trong bộ câu hỏi %(qset_name)s"
         ],
         "Question Set": [
            "Bộ câu hỏi"
         ],
         "Reports": [
            "Xuất báo cáo"
         ],
         "Schools": [
            "Trường"
         ],
         "Index": [
            "Trang chủ"
         ],
         "Contests": [
            "Cuộc thi"
         ],
         "Question sets": [
            "Bộ câu hỏi"
         ],
         "Logout": [
            "Đăng xuất"
         ],
         "Question": [
            "Câu hỏi"
         ],
         "Unknown AJAX error: %(text_status)s %(error_thrown)s, please reload.": [
            "Lỗi AJAX không xác định: %(text_status)s %(error_thrown)s, xin hãy tải lại trang."
         ],
         "Authentication failure": [
            "Lỗi xác thực"
         ],
         "Please login again": [
            "Xin hãy đăng nhập lại"
         ],
         "AJAX error: %(error)s, please reload.": [
            "Lỗi AJAX: %(error)s, xin hãy tải lại trang."
         ],
         "You have finished your exam with score of %(score)d/%(questions_count)d": [
            "Bạn đã hoàn thành bài thi của mình với số điểm %(score)d/%(questions_count)d"
         ],
         "Congratulation": [
            "Chúc mừng"
         ],
         "Error": [
            "Lỗi"
         ],
         "We can't close your exam at the moment. Please try again in a few seconds.": [
            "Chúng tôi không thể kết thúc bài làm của bạn vào thời điểm hiện tại. Xin hãy thử lại trong giây lát."
         ],
         "Do you want to close the exam? You still have time": [
            "Bạn có muốn nộp bài hay không?\nBạn vẫn còn thời gian làm bài."
         ],
         "Do you want to close the exam? You still have time and you have %(question)d questions unanswered": [
            "Bạn có muốn nộp bài hay không?\nBạn vẫn còn thời gian làm bài và còn %(question)d câu hỏi chưa trả lời."
         ],
         "Warning": [
            "Cảnh báo"
         ],
         "Please enter your old password": [
            "Xin hãy nhập mật khẩu cũ của bạn"
         ],
         "Your password is too weak ": [
            "Mật khẩu của bạn quá yếu"
         ],
         "Password either is not the same or too weak": [
            "Mật khẩu hoặc không khớp với ở trên hoặc quá yếu"
         ],
         "Incorrect old password": [
            "Mật khẩu cũ không khớp"
         ],
         "Password changed successfully. You'll be logged out...": [
            "Bạn đã đổi mật khẩu thành công. Bạn sẽ tự động đăng xuất trong giây lát."
         ],
         "Please fill in your name": [
            "Xin hãy điền tên của bạn"
         ],
         "Please choose your school": [
            "Xin hãy chọn trường của bạn"
         ],
         "Please fill in your class": [
            "Xin hãy điền lớp của bạn"
         ],
         "Profile edited successfully": [
            "Thay đổi thông tin thành công"
         ],
         "Error %(error_code)d (%(error_name)s)": [
            "Lỗi %(error_code)d (%(error_name)s)"
         ],
         "Error %(error_code)d": [
            "Lỗi %(error_code)d"
         ],
         "Exam": [
            "Bài thi"
         ],
         "Remaining time": [
            "Thời gian làm bài còn lại"
         ],
         "Answered questions": [
            "Số câu hỏi đã trả lời"
         ],
         "Saving your progress": [
            "Đang lưu bài làm của bạn"
         ],
         "Your progress was saved": [
            "Bài làm của bạn đã được lưu"
         ],
         "Error while saving your progress<br/>Trying...": [
            "Gặp lỗi trong quá trình lưu bài làm<br/>Đang thử lại..."
         ],
         "End exam": [
            "Nộp bài"
         ],
         "Closing exam": [
            "Đang nộp bài"
         ],
         "Announcement published at %(time)s": [
            "Thông báo này được đăng vào lúc %(time)s"
         ],
         "Statistics": [
            "Thống kê"
         ],
         "Account count:": [
            "Số tài khoản đã đăng kí:"
         ],
         "Exam count:": [
            "Số bài thi đã làm:"
         ],
         "Participate": [
            "Tham gia thi"
         ],
         "Password": [
            "Mật khẩu"
         ],
         "Install": [
            "Cài đặt"
         ],
         "minutes": [
            "phút"
         ],
         "seconds": [
            "giây"
         ],
         "Contest: <strong>%(contest_title)s</strong>": [
            "Cuộc thi: <strong>%(contest_title)s</strong>"
         ],
         "This contest starts from <strong>%(begin_date)s</strong> to <strong>%(end_date)s</strong>": [
            "Cuộc thi này bắt đầu từ ngày <strong>%(begin_date)s</strong> tới hết ngày <strong>%(end_date)s</strong>"
         ],
         "Duration: <strong>%(duration)d minutes</strong>": [
            "Thời gian thi: <strong>%(duration)d phút</strong>"
         ],
         "Number of questions: <strong>%(questions)d questions</strong>": [
            "Số câu hỏi: <strong>%(questions)d câu hỏi</strong>"
         ],
         "Your last performance: ": [
            "Thành tích của bạn:"
         ],
         "Attempt": [
            "Lần thi"
         ],
         "Start time": [
            "Thời gian bắt đầu"
         ],
         "End time": [
            "Thời gian kết thúc"
         ],
         "Status": [
            "Trạng thái"
         ],
         "Elapsed time": [
            "Thời gian làm bài"
         ],
         "Score": [
            "Điểm"
         ],
         "Finished": [
            "Đã nộp bài"
         ],
         "Ongoing": [
            "Chưa nộp bài"
         ],
         "Continue": [
            "Tiếp tục làm bài"
         ],
         "Participate in this contest": [
            "Tham gia cuộc thi này"
         ],
         "This contest has either ended or hasn't started": [
            "Cuộc thi hoặc chưa bắt đầu hoặc đã kết thúc"
         ],
         "Rules": [
            "Thể lệ"
         ],
         "Sign in or register": [
            "Đăng nhập hoặc đăng kí"
         ],
         "My profile": [
            "Thông tin bản thân"
         ],
         "Sign out": [
            "Đăng xuất"
         ],
         "Online quiz": [
            "Thi 6 bài học LLCT"
         ],
         "Online quiz Footer": [
            "Thi tìm hiểu 6 Bài học lý luận chính trị dành cho Đoàn viên"
         ],
         "Links": [
            "Các đường dẫn"
         ],
         "Running <strong><a class=\"grey-text text-lighten-3\" href=\"https://github.com/tuankiet65/tracnghiem\">tuankiet65/tracnghiem</a></strong> revision <strong><a class=\"grey-text text-lighten-3\" href=\"https://github.com/tuankiet65/tracnghiem/tree/%(rev)s\">%(rev)s</a></strong>. Current timezone: <strong>%(timezone)s</strong>": [
            "Đang chạy <strong><a class=\"grey-text text-lighten-3\" href=\"https://github.com/tuankiet65/tracnghiem\">tuankiet65/tracnghiem</a></strong>, phiên bản <strong><a class=\"grey-text text-lighten-3\" href=\"https://github.com/tuankiet65/tracnghiem/tree/%(rev)s\">%(rev)s</a></strong>. Múi giờ hiện tại: <strong>%(timezone)s</strong>"
         ],
         "Yourself": [
            "Bản thân"
         ],
         "Your profile": [
            "Thông tin về bạn"
         ],
         "Edit your profile": [
            "Sửa thông tin bản thân"
         ],
         "Change your password": [
            "Thay đổi mật khẩu"
         ],
         "Name": [
            "Họ và tên"
         ],
         "School": [
            "Trường"
         ],
         "Class": [
            "Lớp"
         ],
         "Your past results": [
            "Các kết quả thi của bạn"
         ],
         "Your name": [
            "Tên thật của bạn"
         ],
         "Your school": [
            "Trường của bạn"
         ],
         "Your class": [
            "Lớp của bạn"
         ],
         "Edit profile": [
            "Sửa thông tin"
         ],
         "Old password": [
            "Mật khẩu cũ"
         ],
         "New password": [
            "Mật khẩu mới"
         ],
         "New password (repeat)": [
            "Mật khẩu mới (lặp lại)"
         ],
         "Change password": [
            "Đổi mật khẩu"
         ],
         "Authentication": [
            "Xác thực"
         ],
         "You need to login to proceed": [
            "Bạn cần phải đăng nhập để tiếp tục"
         ],
         "Login": [
            "Đăng nhập"
         ],
         "Register": [
            "Đăng kí"
         ],
         "Your username": [
            "Tên đăng nhập"
         ],
         "Your password": [
            "Mật khẩu"
         ],
         "Don't have an account? Register": [
            "Bạn chưa có tài khoản? Đăng kí"
         ],
         "Incorrect login information, please try again": [
            "Thông tin đăng nhập không chính xác, hãy thử lại"
         ],
         "Password (repeat)": [
            "Nhập lại mật khẩu mới"
         ],
         "Choose your school": [
            "Hãy chọn trường của bạn"
         ],
         "Please wait for all resources to be loaded": [
            "Xin hãy chờ cho các tài nguyên được tải về"
         ],
         "Log in to your existing account": [
            "Đăng nhập vào tài khoản hiện có"
         ],
         "Please fill in your username": [
            "Xin hãy nhập tên đăng nhập"
         ],
         "An account with the same username exists, please choose another username": [
            "Có một tài khoản khác cùng tên đăng nhập với bạn, hãy chọn tên đăng nhập khác."
         ],
         "You haven't filled in some fields, maybe you haven't checked the captcha?": [
            "Bạn chưa điền hết tất cả các trường, có lẽ bạn chưa đánh dấu xác nhận ở dưới cùng?"
         ]
      }
   }
}