appdirs==1.4.3
Babel==2.6.0
bcrypt==3.1.6
blinker==1.4
certifi==2019.3.9
cffi==1.12.3
chardet==3.0.4
Click==7.0
contextlib2==0.5.5
Flask==1.0.2
Flask-Babel==0.12.2
Flask-WTF==0.14.2
itsdangerous==1.1.0
Jinja2==2.10.1
MarkupSafe==1.1.1
packaging==19.0
peewee==2.10.1
pycparser==2.19
PyMySQL==0.9.3
pyparsing==2.4.0
python-dateutil==2.8.0
pytz==2019.1
raven==6.1.0
six==1.12.0
uWSGI==2.0.18
Werkzeug==0.15.2
WTForms==2.2.1
